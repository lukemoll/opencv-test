opencv-test
===

Okay, so there's some weirdness going on.

Clone this repo:  
```
git clone https://lukemoll@bitbucket.org/lukemoll/opencv-test.git
```

Run `run.sh`
```
cd opencv-test/
chmod +x run.sh
./run.sh
```

Does it segfault?