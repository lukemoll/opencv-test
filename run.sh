#!/usr/bin/env bash
set -e
virtualenv --python python3 --no-site-packages env
source env/bin/activate
pip install -r pip-requirements.txt
python imshow.py